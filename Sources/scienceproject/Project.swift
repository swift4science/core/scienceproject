import Foundation

struct Project
{
    var path: URL

    init(projectPath: String)
    {        
        self.path = URL(fileURLWithPath: projectPath, isDirectory: true)
    }

    var modelsPath: URL
    {
        return self.path.appendingPathComponent("models", isDirectory: true) 
    }

    var datasetsPath: URL
    {
        return self.path.appendingPathComponent("datasets", isDirectory: true)
    }

    func dataset(name: String) -> Dataset
    {
        return Dataset(name: name, path: "") 
    }
}
