import XCTest
@testable import scienceproject

final class ProjectTests: XCTestCase 
{
    override func setUp()
    {
        do
        {
            try FileManager.default.removeItem(atPath: "../test_data/project1")
            try FileManager.default.removeItem(atPath: "../test_data/project2")
            try FileManager.default.removeItem(atPath: "../test_data/project3")
        }
        catch
        {
            print("Could not remove directory, error: \(error)")
        }
    }

    override func tearDown()
    {
        do
        {
            try FileManager.default.removeItem(atPath: "../test_data/project1")
            try FileManager.default.removeItem(atPath: "../test_data/project2")
            try FileManager.default.removeItem(atPath: "../test_data/project3")
        }
        catch
        {
            print("Could not remove directory, error: \(error)")
        }
    }

    func testProjectPathSetCorrectly() 
    {
        XCTAssertEqual(URL(fileURLWithPath: "../test_data/project1", isDirectory: true), Project(projectPath: "../test_data/project1").path)
        XCTAssertEqual(URL(fileURLWithPath: "../test_data/project2", isDirectory: true), Project(projectPath: "../test_data/project2").path)
        XCTAssertEqual(URL(fileURLWithPath: "../test_data/project3", isDirectory: true), Project(projectPath: "../test_data/project3").path)
        XCTAssertEqual(URL(fileURLWithPath: "../test_data/project4", isDirectory: true), Project(projectPath: "../test_data/project4").path)
        XCTAssertEqual(URL(fileURLWithPath: "../test_data/project5", isDirectory: true), Project(projectPath: "../test_data/project5").path)
    }

    func testCreatesProjectPathIfNotExists()
    {
        XCTAssertFalse(FileManager.default.fileExists(atPath: "../test_data/project1"))
        let project1 = Project(projectPath:"../test_data/project1")
        XCTAssertTrue(FileManager.default.fileExists(atPath: "../test_data/project1"))
        
        XCTAssertFalse(FileManager.default.fileExists(atPath: "../test_data/project2"))
        let project2 = Project(projectPath:"../test_data/project2")
        XCTAssertTrue(FileManager.default.fileExists(atPath: "../test_data/project2"))

        XCTAssertFalse(FileManager.default.fileExists(atPath: "../test_data/project3"))
        let project3 = Project(projectPath:"../test_data/project3")
        XCTAssertTrue(FileManager.default.fileExists(atPath: "../test_data/project3"))
    }

    func testModelsPathReturnedCorrectly() 
    {
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project1/models", isDirectory: true), Project(projectPath: "test_data/project1").modelsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project2/models", isDirectory: true), Project(projectPath: "test_data/project2").modelsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project3/models", isDirectory: true), Project(projectPath: "test_data/project3").modelsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project4/models", isDirectory: true), Project(projectPath: "test_data/project4").modelsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project5/models", isDirectory: true), Project(projectPath: "test_data/project5").modelsPath)
    }

    func testDatasetsPathReturnedCorrectly() 
    {
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project1/datasets/", isDirectory: true), Project(projectPath: "test_data/project1").datasetsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project2/datasets/", isDirectory: true), Project(projectPath: "test_data/project2").datasetsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project3/datasets/", isDirectory: true), Project(projectPath: "test_data/project3").datasetsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project4/datasets/", isDirectory: true), Project(projectPath: "test_data/project4").datasetsPath)
        XCTAssertEqual(URL(fileURLWithPath: "test_data/project5/datasets/", isDirectory: true), Project(projectPath: "test_data/project5").datasetsPath)
    }
    
    func testDatasetReturnsDatasetWithName()
    {
        let project = Project(projectPath: "test_data/project1")
        
        let dataset1 = project.dataset(name: "dataset1")
        XCTAssertTrue(dataset1 is Dataset)
        XCTAssertEqual("dataset1",dataset1.name)
        
        let dataset2 = project.dataset(name: "dataset2")
        XCTAssertTrue(dataset2 is Dataset)
        XCTAssertEqual("dataset2",dataset2.name)

        let dataset3 = project.dataset(name: "dataset3")
        XCTAssertTrue(dataset3 is Dataset)
        XCTAssertEqual("dataset3",dataset3.name)
    }

    func testDatasetCreatesDatasetDirectoryIfNotExists()
    {        
    }

    static var allTests = [
        ("testProjectPathSetCorrectly", testProjectPathSetCorrectly),
        ("testCreatesProjectPathIfNotExists", testCreatesProjectPathIfNotExists),
        ("testModelsPathReturnedCorrectly", testModelsPathReturnedCorrectly),
        ("testDatasetsPathReturnedCorrectly", testDatasetsPathReturnedCorrectly),
        ("testDatasetReturnsDatasetWithName", testDatasetReturnsDatasetWithName),
        ("testDatasetCreatesDatasetDirectoryIfNotExists", testDatasetCreatesDatasetDirectoryIfNotExists)
    ]
}
