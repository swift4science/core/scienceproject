import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ProjectTests.allTests),
        testCase(DatasetTests.allTests)
    ]
}
#endif
