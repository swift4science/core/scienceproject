import XCTest
@testable import scienceproject

final class DatasetTests: XCTestCase 
{
    func testDatasetSetsName()
    {
        XCTAssertEqual("dataset1", Dataset(name:  "dataset1", path:"").name)
        XCTAssertEqual("dataset2", Dataset(name:  "dataset2", path:"").name)
        XCTAssertEqual("dataset3", Dataset(name:  "dataset3", path:"").name)
        XCTAssertEqual("dataset4", Dataset(name:  "dataset4", path:"").name)
        XCTAssertEqual("dataset5", Dataset(name:  "dataset5", path:"").name)
    }

    func testDatasetSetsDatasetDir() 
    {
        XCTAssertEqual("test_data/project/datasets/dataset1",
                         Dataset(name: "dataset1", path: "test_data/project/datasets/dataset1").path)
        XCTAssertEqual("test_data/project/datasets/dataset2",
                         Dataset(name: "dataset1", path: "test_data/project/datasets/dataset2").path)
        XCTAssertEqual("test_data/project/datasets/dataset3",
                         Dataset(name: "dataset1", path: "test_data/project/datasets/dataset3").path)
        XCTAssertEqual("test_data/project/datasets/dataset4",
                         Dataset(name: "dataset1", path: "test_data/project/datasets/dataset4").path)
        XCTAssertEqual("test_data/project/datasets/dataset5",
                         Dataset(name: "dataset1", path: "test_data/project/datasets/dataset5").path)
    }
    
    static var allTests = [
        ("testDatasetSetsName", testDatasetSetsName),
    ]
}