import XCTest

import scienceprojectTests

var tests = [XCTestCaseEntry]()
tests += scienceprojectTests.allTests()
XCTMain(tests)
